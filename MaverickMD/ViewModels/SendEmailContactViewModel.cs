﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MaverickMD.ViewModels
{
	public class SendEmailContactViewModel
	{
		public string Email { get; set; }
		public string Name { get; set; }
		public string Message { get; set; }
	}
}