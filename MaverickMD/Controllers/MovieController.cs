﻿using MaverickMD.Models;
using MaverickMD.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MaverickMD.Controllers
{
    public class MovieController : Controller
    {
        // GET: Movie/Random
        public ActionResult Random()
        {
			Movie movie = new Movie { Name = "Shrek" };

			List<Customer> Customers = new List<Customer>
			{
				new Customer { Name="John" },
				new Customer { Name="Lucy" }
			};
			
			
			return View(new RandomMovieViewModel { Movie = movie, Customers = Customers });
        }

		public ActionResult Edit(int movieID)
		{
			return Content("id=" + movieID);
		}

		public ActionResult Index(int? pageIndex, string sortBy)
		{
			if (!pageIndex.HasValue)
				pageIndex = 1;

			if (string.IsNullOrWhiteSpace(sortBy))
				sortBy = "Name";

			return Content("PageIndex=" + pageIndex + "&sortBy=" + sortBy);
		}

		[Route("movie/released/{year:regex(\\d{4})}/{month:regex(\\d{2}):range(1, 12)}")]
		public ActionResult Released(int year, int month)
		{
			return Content(year + "/" + month);
		}
    }
}