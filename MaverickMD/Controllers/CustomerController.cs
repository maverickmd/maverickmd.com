﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace MaverickMD.Models
{
    public class CustomerController : Controller
    {
		private ApplicationDbContext _context;

		public CustomerController()
		{
			_context = new ApplicationDbContext();
		}

		protected override void Dispose(bool disposing)
		{
			_context.Dispose();
			base.Dispose(disposing);
		}

		public ActionResult Index()
        {
			IEnumerable<Customer> customers = _context.Customers.Include(c => c.MembershipType).ToList();

			return View(customers);
        }

		[Route("customer/details/{id}")]
		public ActionResult Details(int id)
		{
			Customer customer = _context.Customers.SingleOrDefault(c => c.Id == id);

			if (customer == null)
				return HttpNotFound();

			return View(customer); 
		}
		
    }
}