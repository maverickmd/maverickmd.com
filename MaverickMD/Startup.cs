﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MaverickMD.Startup))]
namespace MaverickMD
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
